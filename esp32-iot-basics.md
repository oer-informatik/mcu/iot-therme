# IoT-Erweiterung der Regelung einer Junker-Therme (Teil 2a: Vorbereitung der Software)

<span class="hidden-text">
https://oer-informatik.de/esp32-iot-heizung-software
</span>

> **tl/dr;** _(ca. 10 min Lesezeit): Für eine Heizungsregelung (ESP32 basierend auf 1-2-4-Bus) sollen Heizungsthermostate (Fritz SmartHome, Dect ULE) ausgelesen werden. Basierend auf den Werten soll die Steuerspannung am Analog-Ausgang des ESP32 eingestellt werden._

Das Gesamtprojekt ist in drei HowTos unterteilt: [1. die Hardware](https://oer-informatik.de/esp32-iot-heizung-hardware), dazwischen dieser Exkurs zur ESP-Vorbereitung und [Teil 2. zur Software](https://oer-informatik.de/esp32-iot-heizung-software).


## Teil 2: die Software

### Vorbereiten des ESP32

Damit die Software vernünftig erstellt werden kann sind ein paar Vorarbeiten nötig. Wer schon mit dem ESP32 gearbeitet hat, kann diesen Teil hier überspringen:

Der ESP32 sollte [installiert und getestet sein ](https://oer-informatik.de/helloworld_esp32). Um ein erstes Gefühl zu erhalten, wie ein Programm mit der Arduino-IDE für den ESP32 geschrieben wird und aus welchen Komponenten es besteht, sollte diese kleine dort verlinkte Blink-Spielerei mal gemacht haben.

Für die folgenden Abschnitte werden jeweils ein Tutuorial verlinkt, was die grundlegenden Schritte erklärt.  Weiterhin werden die relevanten neuen  Codeabschnitte genannt, die für den jeweiligen Bereich zugeschnitten für dieses Projekt sinnvoll sind. Es wird zudem der Ort angegeben, von dem die Codeabschnitte stammen. Dabei werden - wenn nicht genauer spezifiziert - folgende fünf Bereiche unterschieden:

```cpp
/* Section 1 INSERT #include IMPORTS HERE*/

/* Section 2 INSERT VARIABLE DECLARATIONS HERE*/

void setup() {
 /* Section 3 INSERT SETUP-OPERATIONS HERE*/
}

void loop() {
  /* Section 4 INSERT LOOP-OPERATIONS HERE*/
}

/* Section 5 INSERT NEW FUNCTIONS HERE*/
```

### Eine WiFi-Verbindung aufbauen

Eine stabile WiFi-Verbindung ist natürlich Ausgangspunkt für die weitere Kommunikation, da der ESP die Daten der Thermostate von der Fritz-Box per HTTP-Request erhält. Welche Schritte im Allgemeinen nötig sind ist in [diesem Tutorial](https://oer-informatik.de/esp_wifi) beschrieben. Ich benenne hier nur kurz die Codeschnipsel, die im Ergebnis dafür nötig sind:

Zwei Bibliotheken müssen ganz oben, bei den relevanten Imports (Abschnitt 1) ergänzt werden und eine Variable für die Verbindung deklariert werden:
```cpp
#include <WiFi.h>
#include <WiFiMulti.h>
WiFiMulti wifiMulti;
```

Die Zugangsdaten speichere ich immer in einer gesonderten Datei (die nicht mit im Versionscontrollsystem gespeichert wird). Diese muss ebenso per `#include` eingebunden werden. Der Inhalt dieser Datei ist weiter unten abgedruckt. Alternativ könnten die Werte auch direkt im Code eingegeben werden (an Stelle der mit `SECRET_` gekennzeichneten Konstanten). Folgendes muss also in den zweiten Abschnitt (Variablendeklarationen):

```cpp
#include "secrets.h"             // Passwords saved in this file to be hidden from versioncontrol / sharing

//-------------------------------------------------------------------------------------
// WiFi-Settings
//-------------------------------------------------------------------------------------
// WiFi-Settings (if not defined in secrets.h replace your SSID/PW here)
const char*    WIFI_SSID             = SECRET_WIFI_SSID;     // Wifi network name (SSID)
const char*    WIFI_PASSWORD         = SECRET_WIFI_PASSWORD; // Wifi network password

const uint32_t CONNECTION_TIMEOUT_MS = 10000;                // WiFi connect timeout per AP.
const uint32_t MAX_CONNECTION_RETRY  = 20;                   // Reboot ESP after __ times connection errors
```

Eine Datei `secrets.h` muss erstellt werden, die (möglichst) nicht in die Versionskontrolle aufgenommen wird. Sie könnte z.B. so aussehen (natürlich individualisiert auf das eigene WLAN). Anstelle von Konstanten nutze ich hier Präprozessor-Direktiven:

```cpp
#pragma once  // Only run once, even if included multiple times

#define SECRET_WIFI_SSID     "meinWLAN";         // Wifi network name (SSID)
#define SECRET_WIFI_PASSWORD "1234567890123456"; // Wifi network password
```

In der `setup()` (Abschnitt 3) wird die Verbindung konfiguriert und erstmalig aufgebaut:

```cpp
void setup(){
 Serial.begin(115200);                      // Activate debugging via serial monitor
 WiFi.mode(WIFI_STA);                       // Connectmode Station: as client on accesspoint
 wifiMulti.addAP(WIFI_SSID, WIFI_PASSWORD); // multpile networks possible
 ensureWIFIConnection();                    // Call connection-function for the first time
}
```

In der `loop()` (Abschnitt 4) wird immer wieder überprüft, ob eine WLAN Verbindung besteht und diese andernfalls neu aufgebaut:
```cpp
void loop(){
   ensureWIFIConnection();                  // make sure, WiFi is still alive, reboot if necessary
}
```

Die dafür nötige neue Funktion (Abschnitt 5) prüft die Verbindung, baut fehlende Verbindungen auf und bootet bei häufigen Fehlversuchen den ESP einfach neu. Diese Funktion ist so noch nicht lauffähig, da sie bereits die `debugOutput()`-Funktion aufruft, die wir erst unten ergänzen (ggf. die betreffenden Zeilen zum Testen auskommentieren).

```cpp
void ensureWIFIConnection() {
    if (WiFi.status() != WL_CONNECTED) {
       debugOutput("No WIFI Connection found. Re-establishing...", 3, true);
      int connectionRetry = 0;
      while ((wifiMulti.run(CONNECTION_TIMEOUT_MS) != WL_CONNECTED)) {
        delay(1000);
        connectionRetry++;
         debugOutput("WLAN Connection attempt number " + String(connectionRetry), 4, true);
        if (connectionRetry > MAX_CONNECTION_RETRY) {
           debugOutput("Connection Failed! Rebooting...", 4, true);
          delay(5000);
          ESP.restart();
        }
      }
      debugOutput("WiFi is connected", 4, true);
      debugOutput("IP address: " + (WiFi.localIP().toString()), 4, true);
      debugOutput("Connected to (SSID): " + String(WiFi.SSID()), 5, true);
      debugOutput("Signal strength (RSSI): " + String(WiFi.RSSI()) + "(-50 = perfect / -100 no signal)", 5, true);
    }
  }
```

Die Funktion prüft, ob eine WLAN-Verbindung exitiert, baut sie ggf. wieder auf und startet den ESP bei mehreren Fehlversuchen neu. Bei früheren Projekten musste ich immer mal den Strom abklemmen, um einen Neustart herbeizuführen - diese Variante läuft bei mir stabil (bzw. startet sich selbständig neu).

### Over the Air (OTA) Updates

Dieser Abschnitt ist optional. Ich finde es bei allen fertigen Projekten sehr hilfreich, [über WLAN neue Firmware](https://oer-informatik.de/esp_ota) aufspielen zu können. Vor allem bei Geräten, die nicht mobil sind und über keine dauerhafte USB-Verbindung verfügen kann ich das nur empfehlen.

Die Funktionalität stellt der relevanten Import `ArduinoOTA.h` bereit. Es sind noch ein paar frei wählbare Konfigurationswerte nötig. Die Werte der Variablen müssen wieder (wie bei den WLAN Credentials auch) in der Datei secrets.h übergeben werden. Die Flag `ENABLE_UPDATE_JUMPER` dient dazu, OTA per Jumper/Schalter deaktivieren zu können:

```cpp
/-------------------------------------------------------------------------------------
// Over-the-Air Update (Upload new Software via WiFi)
// OTA-Password-Settings (if not defined in secrets.h replace your SSID/PW here)
// Anleitung z.B. hier: https://oer-informatik.de/esp_ota
//-------------------------------------------------------------------------------------
#include <ArduinoOTA.h>
#include <ESPmDNS.h>
#include <WiFiUdp.h>
const char* OTA_HOSTNAME = SECRET_OTA_HOSTNAME; // Name of device for over-the-air-updates (OTA)
const char* OTA_PASSWORD = SECRET_OTA_PASSWORD; // Password for over-the-air-updates (OTA)

const bool ENABLE_UPDATE_JUMPER = false;
```


Daraus folgt, dass auch die `secrets.h` erweitert werden muss um folgende Eingaben (kann beliebig individualisiert werden - sollte nur bekannt sein, um beides in der Arduino-IDE bei einem WLAN-Update eingeben zu können):
```cpp
#define SECRET_OTA_HOSTNAME "WLANHeizungsSteuerung";
#define SECRET_OTA_PASSWORD  "sdfklgjfklgjfdl";
```

Ich möchte die OTA Funktion per Schalter deaktivieren können, daher muss ich dafür noch einen Pin definieren:

```cpp
//-------------------------------------------------------------------------------------
// List of Input- and Output-devices and Pins
//-------------------------------------------------------------------------------------
// Datatype | Name of Variable    | Pin No. connected | Name, Behaviour*/
const int     PIN_UPDATE_ACTIVE   = 4;               // Pullup, HIGH = Update active
```


Nach der Aktivierung der WLAN-Verbindung muss der OTA-Service aktiviert werden. Hierzu muss in der Die  `setup()` nach `ensureWIFIConnection();` eingegeben werden (Abschnitt 3):
```cpp
void setup(){
  pinMode(PIN_UPDATE_ACTIVE, INPUT_PULLUP);
  ...
   startOTA();
}
```

In der `loop()` (Abschnitt 4) wird dann bei jedem Durchlauf geprüft, ob ein neues Update installiert werden will. Ggf. kann hilfreich sein, diese Funktion über einen Jumper/Taster/Schalter freizuschalten.

```cpp
void loop(){
  if ((digitalRead(PIN_UPDATE_ACTIVE) == HIGH)||(!ENABLE_UPDATE_JUMPER)){ // UPDATE Jumper not set or flag set
    ArduinoOTA.handle();
  }
    ....
}
```

Was fehlt ist die Funktion `startOTA()`. Diese habe ich komplett aus den Beispielen der Bibliothek übernommen, lediglich die `debugOutput()` Funktion eingefügt - daher ist es auch erst lauffähig, wenn diese implementiert wurde (siehe unten).

Die relevanten neuen Funktionen (Abschnitt 5):
```cpp
void startOTA() {
  ArduinoOTA.onStart([]() {
      String type;
      if (ArduinoOTA.getCommand() == U_FLASH)
        type = "sketch";
      else  // U_SPIFFS
        type = "filesystem";
      // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
      debugOutput("Start OTA Firmware update " + type, 4, true);
    });

  ArduinoOTA.onEnd([]() {
      debugOutput("\nEnd OTA Firmware update ", 4, true);
    });

  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
      debugOutput("OTA Progress: " + String(progress / (total / 100)), 4, true);
    });

  ArduinoOTA.onError([](ota_error_t error) {
      debugOutput("Error " + String(error), 4, true);
      if (error == OTA_AUTH_ERROR)         debugOutput("Auth Failed", 2, true);
      else if (error == OTA_BEGIN_ERROR)   debugOutput("Begin Failed", 2, true);
      else if (error == OTA_CONNECT_ERROR) debugOutput("Connect Failed", 2, true);
      else if (error == OTA_RECEIVE_ERROR) debugOutput("Receive Failed", 2, true);
      else if (error == OTA_END_ERROR)     debugOutput("End Failed", 2, true);
    });

  ArduinoOTA.setHostname(OTA_HOSTNAME);
  ArduinoOTA.setPassword(OTA_PASSWORD);
  ArduinoOTA.begin();
}
```

Klappt das Hochladen einer neuen Version per WLAN? Fein.

### Loggen der Werte und Debugging-Meldungen

Bei so einem Prototyp gibt es immer viel nachzubessern. Besser, man hat auch im eingebauten Zustand einen Weg, an Log-Dateien zu kommen. Ich löse das über einen Webserver, das setzt natürlich die funktionierende WLAN-Verbindung voraus (aber die sollte ja bereits bestehen). Detailiert habe ich das [in diesem Artikel](https://oer-informatik.de/esp_debug) beschrieben. Das Beispiel hier ist abgekürzt. Sinnvoll erscheint es z.B. (wie auch oben bei OTA) über einen Jumper oder ähnliches das Debugging zu aktivieren.

Es werden in Abschnitt 1 und 2 neue Imports für den Webserver und den Zeitserver benötigt und Webserver, Zeitserver und Loglevel konfiguriert werden:

```cpp
//-------------------------------------------------------------------------------------
// Configuration of the NTP-Server
//-------------------------------------------------------------------------------------

#include "time.h"
const char* NTP_SERVER = "pool.ntp.org";
const long GMT_OFFSET_SEC = 3600;
const int DAYLIGHT_OFFSET_SEC = 3600;

//-------------------------------------------------------------------------------------
// Set Route and Port for the Logpage-Webserver
//-------------------------------------------------------------------------------------

#include <WebServer.h>
const int WEBSERVER_PORT = 8085;
const char* WEBSERVER_ROUTE_TO_DEBUG_OUTPUT = "/log";

WebServer server(WEBSERVER_PORT);
String setupLogText = "";
String loopLogText = "";

//-------------------------------------------------------------------------------------
// Logging to serial console?
// If following line is commentet ("//#define DEBUG") all logging-operations will be
// replaced by "", otherwise if "#define DEBUG" is present logging will be sent to serial
//-------------------------------------------------------------------------------------

#define DEBUG  //Flag to activate logging to serial console (i.e. serial monitor in arduino ide)

#ifdef DEBUG
#define DEBUG_PRINT(x) Serial.print(x)
#define DEBUG_PRINTLN(x) Serial.println(x)
#else
#define DEBUG_PRINT(x)
#define DEBUG_PRINTLN(x)
#endif


//-------------------------------------------------------------------------------------
// LogLevels used in this example. Only entries bigger than LOG_LEVEL will be written
//-------------------------------------------------------------------------------------

String LOG_LEVEL_NAMES[] = {"OFF", "FATAL", "ERROR", "WARN", "INFO", "DEBUG", "TRACE", "ALL"};
const int MIN_LOG_LEVEL = 7;
```

Ich möchte Logging per Schalter aktivieren/deaktivieren können - muss daher einen Pin definieren:

```cpp
//-------------------------------------------------------------------------------------
// List of Input- and Output-devices and Pins
//-------------------------------------------------------------------------------------
// Datatype | Name of Variable    | Pin No. connected | Name, Behaviour*/
const int     PIN_LOGGING_ACTIVE  = 4;               // Pullup, HIGH = Logging active (Same as UPDATE)
```
In der  `setup()` (Abschnitt 3) wird schließlich der Zeitserver konfiguriert und der Webserver gestartet:

```cpp
void setup(){
  #ifdef DEBUG
  Serial.begin(115200); // Activate debugging via serial monitor
  #endif

  pinMode(PIN_LOGGING_ACTIVE, INPUT_PULLUP);

  //... ensureWIFIConnection usw.

  // Init and get the time
  configTime(GMT_OFFSET_SEC, DAYLIGHT_OFFSET_SEC, NTP_SERVER);
  debugOutput("Starting Webserver...", 6, true);
  server.on(WEBSERVER_ROUTE_TO_DEBUG_OUTPUT, respondRequestWithLogEntries);
  String log_address = "http://"+WiFi.localIP().toString() + ":" + String(WEBSERVER_PORT) + WEBSERVER_ROUTE_TO_DEBUG_OUTPUT;
  debugOutput("Logging will be published on: "+log_address , 5, true);
  server.begin();
}
```

In der `loop()` (Abschnitt 4) wird schließlich nach OTA und `ensureWIFIConnection();` abgefragt, ob es Website-Anfragen (GET-Requests) gibt, die beantwortet werden müssen.

```cpp
void loop(){
  ... // z.B. ensureWIFIConnection();
  if (digitalRead(PIN_LOGGING_ACTIVE) == HIGH){ // LOGGING Jumper not set or flag set
   server.handleClient();
 }
}
```

Das Logging läuft komplett über eine neue Funktion (in Abschnitt 5). Im restlichen Code muss jetzt statt direkt `Serial.println()` aufzurufen eine der drei überladenen `debugOutput()` stehen.

```cpp
void debugOutput(String text, int logLevel, bool setupLog) {
  if (MIN_LOG_LEVEL >= logLevel) {
    String timeAsString = "";    
    struct tm timeinfo;
    if (!getLocalTime(&timeinfo)) {
        timeAsString = "[no NTP]";
    }else{
      char timeAsChar[20];
      strftime(timeAsChar, 20, "%Y-%m-%d_%H:%M:%S", &timeinfo);
      timeAsString = String(timeAsChar);
    }
    if (setupLog) {
      setupLogText = setupLogText + "[" +  timeAsString + "] " + " ["+LOG_LEVEL_NAMES[logLevel]+ "] " + text + "<br/>\n";
    } else {
      loopLogText = loopLogText + "[" +  timeAsString + "] " + " ["+LOG_LEVEL_NAMES[logLevel]+ "] "+ text + "<br/>\n";
    }
    DEBUG_PRINTLN("["+timeAsString + "] ["+LOG_LEVEL_NAMES[logLevel]+ "] " + text);
  }
}


void debugOutput(String text, int logLevel) {
  debugOutput(text, logLevel, false); // log to loopLogText is default
}


void debugOutput(String text) {
  debugOutput(text, 4); // no loglevel present? use "INFO"
}
```

Was fehlt? Die Datei, die den Inhalt der Website erstellt und die _response_ absendet (`respondLogfile()`) und eine Datei, die das alles in ein HTML-Dokument verpackt (`renderHtml()`).

```cpp
String renderHtml(String header, String body) {
  // HTML & CSS contents which display on web server
  String html = "";
  html = html + "<!DOCTYPE html>\n<html>\n"+"<html lang='de'>\n<head>\n<meta charset='utf-8'>\n<title>"+header+"</title>\n</head><body>\n<h1>";
  html = html + header + "</h1>\n";
  html = html + body + "\n</body>\n</html>\n";
  return html;
}

void respondRequestWithLogEntries() {
  String header = "Debugging-Log-Entries";
  String body = "";
  body = "<h2>Logging on Startup / during configuration (Setup-Log)</h2>\n";
  body = body + setupLogText;
  body = body + "<h2>Logging during operation (Loop-Log)</h2>\n";
  body = body + loopLogText;
  server.send(200, "text/html; charset=utf-8", renderHtml(header, body));
}
```

So, das waren alles Vorarbeiten. Jetzt fangen wir überhaupt erst an mit der Heizungsteuerung:

[Weiter zu Teil 2: Software](https://oer-informatik.de/esp32-iot-heizung-software)

## _Quellen und offene Ressourcen (OER)_

Die Ursprungstexte (als Markdown), Grafiken und zugrunde liegende Diagrammquelltexte finden sich (soweit möglich in weiterbearbeitbarer Form) in folgendem git-Repository:

[https://gitlab.com/oer-informatik/mcu/iot-therme](https://gitlab.com/oer-informatik/mcu/iot-therme).

Sofern nicht explizit anderweitig angegeben sind sie zur Nutzung als Open Education Resource (OER) unter Namensnennung (H. Stein, oer-informatik.de) freigegeben gemäß der [Creative Commons Namensnennung 4.0 International Lizenz (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/deed.de).

[![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by/4.0/88x31.png)](https://creativecommons.org/licenses/by/4.0/deed.de)
