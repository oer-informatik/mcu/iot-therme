#pragma once                                             // Only run once, even if included multiple times


#define SECRET_WIFI_SSID     "meinWLAN";         // Wifi network name (SSID)
#define SECRET_WIFI_PASSWORD "1234567890123456"; // Wifi network password

#define SECRET_FUSER "fritz_smart_home_nutzer";   // The username if you created an account, "admin" otherwise
#define SECRET_FPASS "hdffds89fwnfwlfksdo3IÖN";  // The password for the aforementioned account.
#define SECRET_IP "192.168.179.1";                // IP address of your router. This should be "192.168.179.1" for most FRITZ!Boxes
#define SECRET_PORT 49000;                        // Port of the API of your router. This should be 49000 for all TR-064 devices.


#define SECRET_OTA_HOSTNAME "WLANHeizungsSteuerung";
#define SECRET_OTA_PASSWORD  "sdfklgjfklgjfdl";