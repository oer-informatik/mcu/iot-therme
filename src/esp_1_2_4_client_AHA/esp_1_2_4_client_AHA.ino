//-------------------------------------------------------------------------------------
// Load WiFi-Libraries
//-------------------------------------------------------------------------------------

#include <WiFi.h>
#include <WiFiMulti.h>
WiFiMulti wifiMulti;


//-------------------------------------------------------------------------------------
// All Credentials are stored in file secrets.h (must be created using secrets_EXAMPLE.h)
// => add line "*secrets.h" to .gitignore to prevent publishing credentials to repository
//-------------------------------------------------------------------------------------

#include "secrets.h"             // Passwords saved in this file to be hidden from versioncontrol and sharing


//-------------------------------------------------------------------------------------
// WiFi-Settings (if not defined in secrets.h replace your SSID/PW here)
//-------------------------------------------------------------------------------------

const char*    WIFI_SSID             = SECRET_WIFI_SSID;     // Wifi network name (SSID)
const char*    WIFI_PASSWORD         = SECRET_WIFI_PASSWORD; // Wifi network password

const uint32_t CONNECTION_TIMEOUT_MS = 10000;               // WiFi connect timeout per AP.
const uint32_t MAX_CONNECTION_RETRY  = 20;                  // Reboot ESP after __ times connection errors



//-------------------------------------------------------------------------------------
// Configuration of the NTP-Server
//-------------------------------------------------------------------------------------

#include "time.h"
const char* NTP_SERVER = "pool.ntp.org";
const long GMT_OFFSET_SEC = 3600;
const int DAYLIGHT_OFFSET_SEC = 3600;



//-------------------------------------------------------------------------------------
// Set Route and Port for the Logpage-Webserver
//-------------------------------------------------------------------------------------

#include <WebServer.h>
const int WEBSERVER_PORT = 8085;
const char* WEBSERVER_ROUTE_TO_DEBUG_OUTPUT = "/log";

WebServer server(WEBSERVER_PORT);
String setupLogText = "";
String loopLogText = "";

//-------------------------------------------------------------------------------------
// Logging to serial console? 
// If following line is commentet ("//#define DEBUG") all logging-operations will be
// replaced by "", otherwise if "#define DEBUG" is present logging will be sent to serial 
//-------------------------------------------------------------------------------------

#define DEBUG  //Flag to activate logging to serial console (i.e. serial monitor in arduino ide)

#ifdef DEBUG
#define DEBUG_PRINT(x) Serial.print(x)
#define DEBUG_PRINTLN(x) Serial.println(x)
#else
#define DEBUG_PRINT(x)
#define DEBUG_PRINTLN(x)
#endif


//-------------------------------------------------------------------------------------
// LogLevels used in this example. Only entries bigger than LOG_LEVEL will be written
//-------------------------------------------------------------------------------------

String LOG_LEVEL_NAMES[] = {"OFF", "FATAL", "ERROR", "WARN", "INFO", "DEBUG", "TRACE", "ALL"};
const int MIN_LOG_LEVEL = 4;


//-------------------------------------------------------------------------------------
// Over-the-Air Update (Upload new Software via WiFi)
// OTA-Password-Settings (if not defined in secrets.h replace your SSID/PW here)
//-------------------------------------------------------------------------------------
#include <ArduinoOTA.h>
#include <ESPmDNS.h>
#include <WiFiUdp.h>

const char* OTA_HOSTNAME = SECRET_OTA_HOSTNAME; // Name of device for over-the-air-updates (OTA)
const char* OTA_PASSWORD = SECRET_OTA_PASSWORD; // Password for over-the-air-updates (OTA)

const bool ENABLE_UPDATE_JUMPER = false;


//-------------------------------------------------------------------------------------
// List of Input- and Output-devices and Pins
//-------------------------------------------------------------------------------------
// Datatype | Name of Variable    | Pin No. connected | Name, Behaviour*/
const int     PIN_UPDATE_ACTIVE   = 4;               // Pullup, HIGH = Update active
const int     PIN_LOGGING_ACTIVE  = 4;               // Pullup, HIGH = Logging active (Same as UPDATE)
const int     DAC_THERMOSTAT      = 25;              // DAC setting V for OP-circuit (heating)
const int     PIN_OPERATING_LED   = 23;              // green LED, HIGH-active
const int     PIN_HEATING_MAX     = 32;              // red LED, HIGH-active
const int     PIN_HEATING_ON      = 33;              // yellow LED, HIGH-active
const int     PIN_HEATING_OFF     = 22;              // blue LED, HIGH-active

//-------------------------------------------------------------------------------------
// App-Einstellungen
//-------------------------------------------------------------------------------------
const float MAX_TEMP = 21;              // Höchsttemperatur, auf die die Heizung ausgelegt werden soll
const float MIN_TEMP = 8;               // Mindestemperatur, auf die geheizt wird
const int CHECK_MILLISECONDS = 90000;  // Intervall in Millisekunden, in denen neue Werte eingelesen werden
int currentMillis = 0;

#ifndef SECRETSINCLUDED
// Die AIN (Actor Identificaion Number) der Devices findet sich im FritzBox Webinterface
const String ain_wohnzimmer   = "12345 9876541";
const String ain_kueche       = "12345 9876542";
const String ain_bad          = "12345 9876543";
const String ain_esszimmer    = "12345 9876544";
const String ain_schlafzimmer = "12345 9876545";
String HEIZKOERPERREGLER[] = { ain_wohnzimmer, ain_kueche, ain_bad, ain_esszimmer, ain_schlafzimmer};
int anzahlHKR = (sizeof(HEIZKOERPERREGLER) / sizeof(HEIZKOERPERREGLER[0]));
#endif

const char* fuser = SECRET_FUSER;          // The username if you created an account, "admin" otherwise
const char* fpass = SECRET_FPASS;          // The password for the aforementioned account.

// -------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------
// AHA ruft per HTTPClient die Daten ab
//-------------------------------------------------------------------------------------
#include <HTTPClient.h>


void setup(){

  // START LOGGING EXAMPLE... REPLACE WITH YOUR OWN CODE
  debugOutput("Starting Programm...", 6, true);
  #ifdef DEBUG
  Serial.begin(115200); // Activate debugging via serial monitor
  #endif

  pinMode(PIN_OPERATING_LED, OUTPUT);
  pinMode(PIN_HEATING_OFF, OUTPUT);
  pinMode(PIN_HEATING_ON, OUTPUT);
  pinMode(PIN_HEATING_MAX, OUTPUT);
  pinMode(PIN_UPDATE_ACTIVE, INPUT_PULLUP);
  pinMode(PIN_LOGGING_ACTIVE, INPUT_PULLUP);

  digitalWrite(PIN_OPERATING_LED, HIGH);
  digitalWrite(PIN_HEATING_OFF, HIGH);

  // Startwert: DAC = 34 für ca. 0,45V also 5V hinter OPV
  dacWrite(DAC_THERMOSTAT, 34);
  
  debugOutput("WiFi will be established", 6, true);
  WiFi.mode(WIFI_STA);                       // Connectmode Station: as client on accesspoint
  wifiMulti.addAP(WIFI_SSID, WIFI_PASSWORD); // multpile networks possible
  debugOutput("Connecting to a WiFi Accesspoint", 5, true);
  ensureWIFIConnection();                    // Call connection-function for the first 
  
   // Init and get the time
  debugOutput("Connection to NTP-Timeserver", 6, true);
  configTime(GMT_OFFSET_SEC, DAYLIGHT_OFFSET_SEC, NTP_SERVER);

  debugOutput("Starting Webserver...", 6, true);
  server.on(WEBSERVER_ROUTE_TO_DEBUG_OUTPUT, respondRequestWithLogEntries);
  String log_address = "http://"+WiFi.localIP().toString() + ":" + String(WEBSERVER_PORT) + WEBSERVER_ROUTE_TO_DEBUG_OUTPUT;
  debugOutput("Logging will be published on: "+log_address , 5, true);
  server.begin();

  debugOutput("Starting OTA Firmware Updateserver...", 6, true);
  digitalWrite(PIN_HEATING_OFF, LOW);
  digitalWrite(PIN_HEATING_ON, HIGH);

  startOTA();

  digitalWrite(PIN_HEATING_MAX, HIGH);
  digitalWrite(PIN_HEATING_ON, LOW);

  debugOutput("Finished startup.", 6, true);
  digitalWrite(PIN_HEATING_MAX, LOW);
  digitalWrite(PIN_OPERATING_LED, LOW);
}

void loop(){
  ensureWIFIConnection();
  if ((digitalRead(PIN_UPDATE_ACTIVE) == HIGH)||(!ENABLE_UPDATE_JUMPER)){ // UPDATE Jumper not set or flag set
    ArduinoOTA.handle();
  }
  if (digitalRead(PIN_LOGGING_ACTIVE) == HIGH){ // LOGGING Jumper not set or flag set
    server.handleClient(); 
  }
  if (((millis() - currentMillis) > CHECK_MILLISECONDS) || (currentMillis == 0)) {
    loopLogText = "";
    digitalWrite(PIN_OPERATING_LED, HIGH); //zeigt an, dass er TempDiff abfragt
    float sollTemp = 0; // alle Variablen zurücksetzen
    float istTemp = 0;
    float maxDiff = 0;
    float hkrDiff = 0;
    String hkrName = "";

    HTTPClient http;
    String sessionID = getNewLoginSessionID(http);

    for (int i = 0; i < anzahlHKR; i++) { 
      sollTemp = getAhaAttribute("hkrtsoll", HEIZKOERPERREGLER[i], sessionID, http).toFloat() / 2;
      istTemp = getAhaAttribute("temperature", HEIZKOERPERREGLER[i], sessionID, http).toFloat() / 10;
      hkrDiff = relevantTempDiff(istTemp, sollTemp);

      hkrName = getAhaAttribute("switchname", HEIZKOERPERREGLER[i], sessionID, http);
      debugOutput("Name: " + String(hkrName) + " / AHA-Soll: " + String(sollTemp) + " / AHA-Ist: " + String(istTemp), 4);
      if (hkrDiff > maxDiff) {
        maxDiff = hkrDiff;
        debugOutput("Neue Maximaldifferenz: " + String(maxDiff), 4);
      }
      // Beispielhaft: wie würde das Schreiben der Temperaturen aussehen?
      // int newTemp = 11;
      // setAhaAttribute( "hkrtsoll", String(int(newTemp * 2)), HEIZKOERPERREGLER[i], sessionID, http);
    }

    ahaLogOut(sessionID, http);
    
    writeTemperature(maxDiff);
    digitalWrite(PIN_OPERATING_LED, LOW);
    currentMillis = millis();
  }
}


String getNewLoginSessionID(HTTPClient& http) {

  String sessionId= "";
  debugOutput("Starte AHA Session ", 5);

  String challenge = getAhaChallenge(http);
  if (challenge != "") {
    String response = getCalculatedResponse(challenge);

    sessionId = getAhaSessionID(response, http);
  }

  return sessionId;
}


String getAhaChallenge(HTTPClient& http) {
  // Get Challenge
  http.begin("http://fritz.box/login_sid.lua");
  int retCode = http.GET();
  if (retCode != 200) {
    debugOutput("[AHA] Get Challenge failed! " + String(retCode), 3);
    return "";
  }
  debugOutput("[AHA] Getting Challenge  ", 6);
  String result = http.getString();
  String challenge = result.substring(result.indexOf("<Challenge>") + 11, result.indexOf("<Challenge>") + 19);
  return challenge;
}


String getCalculatedResponse(String challenge) {
  // Calculate Response
  debugOutput("[AHA] Calculating response ", 6);
  String reponseASCII = challenge + "-" + fpass;
  String responseHEX = "";
  for (unsigned int i = 0; i < reponseASCII.length(); i++) {
    responseHEX = responseHEX + String(reponseASCII.charAt(i), HEX) + "00";
  }

  MD5Builder md5;
  md5.begin();
  md5.addHexString(responseHEX);
  md5.calculate();

  String response = challenge + "-" + md5.toString();
  return response;
}

String getAhaSessionID(String response, HTTPClient& http) {
  // Login and get SID
  debugOutput("[AHA] Get Session ID ", 6);
  http.begin("http://fritz.box/login_sid.lua?user=" + String(fuser) + "&response=" + response);
  int retCode = http.GET();
  if (retCode != 200) {
    debugOutput("[AHA] Get SessionID failed! " + String(retCode), 3);
    return "";
  }
  String result = http.getString();
  String sid = result.substring(result.indexOf("<SID>") + 5, result.indexOf("<SID>") + 21);
  debugOutput("[AHA] Session established", 6);
  return sid;
}

void ahaLogOut(String sid, HTTPClient& http) {
  http.begin("http://fritz.box/login_sid.lua?logout=1&sid=" + sid);
  http.GET();
  String result = http.getString();
  http.end();
  //debugOutput("Antwort auf Logout:");
  //debugOutput(result);
}

String getAhaAttribute(String command, String AIN, String sid, HTTPClient& http) {
  String ainparam = "";
  if (AIN != "") {
    ainparam = "&ain=" + AIN;
  }
  String request = "http://fritz.box/webservices/homeautoswitch.lua?switchcmd=get" + command + ainparam + "&sid=" + sid;
  http.begin(request);
  int retCode = http.GET();
  if (retCode != 200) {
    debugOutput("[AHA] Den Wert " + command + " zu lesen (" + request + ") ist gescheitert mit Fehlercode:  " + String(retCode), 3);
    return "";
  }
  String result = http.getString();

  debugOutput("[AHA] Lese "+command+": " + result, 6);
  return result;
}


bool setTempAhaoInSingleSession(String AIN, String command, float temp) { 
  //schreibt in einem Rutsch die neue Solltemperatur
  // benötigt keine geöffnete Session
  
  HTTPClient http;
  debugOutput("Stelle Temp ein auf " + String(temp) + " für " + command, 5);

  String challenge = getAhaChallenge(http);
  if (challenge != "") {
    String response = getCalculatedResponse(challenge);

    String sid = getAhaSessionID(response, http);
    if (sid == "") {
      return false;
    }

    String param = String(int(temp * 2));
    setAhaAttribute(command, param, AIN, sid, http);
    ahaLogOut(sid, http);
  }
  return true;
}


String setAhaAttribute(String command, String param, String AIN, String sid, HTTPClient& http) {

  http.begin("http://fritz.box/webservices/homeautoswitch.lua?switchcmd=set" + command + "&ain=" + AIN + "&param=" + param + "&sid=" + sid);
  int retCode = http.GET();
  if (retCode != 200) {
    debugOutput("Den Wert " + command + " auf " + param + " setzen ist gescheitert mit Fehlercode:  " + String(retCode), 3);
    //return "";
  }
  String result = http.getString();

  debugOutput("Antwort des Schreibens :" + result, 5);
  return result;
}

String getAhaInfoInSingleSession(String AIN, String command) {
  //schreibt in einem Rutsch die neue Solltemperatur
  // benötigt keine geöffnete Session
  HTTPClient http;
  String result = "";
  debugOutput("Hole AHA-Info fuer " + command, 5);

  String challenge = getAhaChallenge(http);
  if (challenge != "") {
    String response = getCalculatedResponse(challenge);

    String sid = getAhaSessionID(response, http);
    if (sid == "") {
      return "";
    }

    if (command == "switchlist") {
      result = getAhaAttribute(command, "", sid, http);
    } else {
      result = getAhaAttribute(command, AIN, sid, http);
    }

    ahaLogOut(sid, http);
  }
  return result;
}

void writeTemperature(float tempDiff) {
  /*Gemessene Werte am Juncker-Raumthermostat bei eingestellte Raumtemp 18°C:
  - Raum 1°C oder mehr wärmer als eingestellte SollTemperatur: 340mV
  - wenn der Raum nur 0,5°C wärmer ist alls SollTemperatur springt das Thermostat auf 9-10V
  - bei Solltemp = IstTemp ca. 11-15V
  - Raum 0.5°C zu kalt: 18-20V
  - ab 1°C zu kalt: 20V
  */
  byte minDACValue = 0;
  byte maxDACValue = 255;
  float minVoltageValue = 0;
  float maxVoltageValue = 3.3;
  float voltageHeatingOff = 0.3;    // aus (5V nach OPV) - Das Orginal Raumthermostat (ORT) geht auf 300mV
  float voltageHeatingStart = 0.7;  // start Regelung (8V nach OPV) - Das ORT startet mit 10V bei
  float voltageHeatingMax = 1.9;    // Regelung voll (20V nach OPV)
  float tempDiffMax = 4;
  float tempDiffMin = 0;
  //  2.1V: Sättigung (22V)
  float setVoltage = voltageHeatingOff;
  digitalWrite(PIN_HEATING_OFF, LOW);
  digitalWrite(PIN_HEATING_ON, LOW);
  digitalWrite(PIN_HEATING_MAX, LOW);
  if (tempDiff <= 0.2) {
    setVoltage = voltageHeatingOff;
    debugOutput("Heizung aus", 5);
    digitalWrite(PIN_HEATING_OFF, HIGH);

  } else {
    if (tempDiff > tempDiffMax) {
      setVoltage = voltageHeatingMax;
      debugOutput("Heizung max", 5);
      digitalWrite(PIN_HEATING_MAX, HIGH);
    } else {
      setVoltage = ((tempDiff - tempDiffMin) * (voltageHeatingMax - voltageHeatingStart) / (tempDiffMax - tempDiffMin) + voltageHeatingStart);
      digitalWrite(PIN_HEATING_ON, HIGH);
    }
  }
  byte dacValue = byte(((setVoltage - minVoltageValue) * (maxDACValue - minDACValue) / (maxVoltageValue - minVoltageValue) + minDACValue));

  dacWrite(DAC_THERMOSTAT, dacValue);
  debugOutput("Stelle den DAC-Wert auf " + String(dacValue), 4);
  debugOutput(" fuer eine TempDiff von " + String(tempDiff) + " K", 4);
  debugOutput(" fuer eine Spannung von " + String(setVoltage) + " V", 4);
}

float relevantTempDiff(float hkrIsTemp, float hkrSetTemp) {
  float tempDiff = 0;

  //grobe Plausibilisierung der Werte

  if ((hkrIsTemp > -15) && (hkrIsTemp < 45) && (hkrSetTemp > -15) && (hkrSetTemp < 256)) {

    float usedSetTemp = hkrSetTemp;
    if (int(hkrSetTemp * 10) == 1265) {
      usedSetTemp = MIN_TEMP;
    } else if (hkrSetTemp > MAX_TEMP) {
      usedSetTemp = MAX_TEMP;
    }
    tempDiff = (usedSetTemp-0.5)-hkrIsTemp;

  } else {
    debugOutput("Etwas stimmt mit den Werten nicht", 3);
  }
  return tempDiff;
}

void debugOutput(String text, int logLevel, bool setupLog) {
  if (MIN_LOG_LEVEL >= logLevel) {
    String timeAsString = "";    
    struct tm timeinfo;
    if (!getLocalTime(&timeinfo)) {
        timeAsString = "[no NTP]";
    }else{
      char timeAsChar[20];
      strftime(timeAsChar, 20, "%Y-%m-%d_%H:%M:%S", &timeinfo);
      timeAsString = String(timeAsChar);
    }
    if (setupLog) {
      setupLogText = setupLogText + "[" +  timeAsString + "] " + " ["+LOG_LEVEL_NAMES[logLevel]+ "] " + text + "<br/>\n";
    } else {
      loopLogText = loopLogText + "[" +  timeAsString + "] " + " ["+LOG_LEVEL_NAMES[logLevel]+ "] "+ text + "<br/>\n";
    }
    DEBUG_PRINTLN("["+timeAsString + "] ["+LOG_LEVEL_NAMES[logLevel]+ "] " + text);
  }
}


void debugOutput(String text, int logLevel) {
  debugOutput(text, logLevel, false); // log to loopLogText is default
}


void debugOutput(String text) {
  debugOutput(text, 4); // no loglevel present? use "INFO"
}


void ensureWIFIConnection() {
    if (WiFi.status() != WL_CONNECTED) {
       debugOutput("No WIFI Connection found. Re-establishing...", 3, true);
      int connectionRetry = 0;
      while ((wifiMulti.run(CONNECTION_TIMEOUT_MS) != WL_CONNECTED)) {
        delay(1000);
        connectionRetry++;
         debugOutput("WLAN Connection attempt number " + String(connectionRetry), 4, true);
        if (connectionRetry > MAX_CONNECTION_RETRY) {
           debugOutput("Connection Failed! Rebooting...", 4, true);
          delay(5000);
          ESP.restart();
        }
      }
      debugOutput("WiFi is connected", 4, true);
      debugOutput("IP address: " + (WiFi.localIP().toString()), 4, true);
      debugOutput("Connected to (SSID): " + String(WiFi.SSID()), 5, true);
      debugOutput("Signal strength (RSSI): " + String(WiFi.RSSI()) + "(-50 = perfect / -100 no signal)", 5, true);
    }
  }


String renderHtml(String header, String body) {
  // HTML & CSS contents which display on web server
  String html = "";
  html = html + "<!DOCTYPE html>\n<html>\n"+"<html lang='de'>\n<head>\n<meta charset='utf-8'>\n<title>"+header+"</title>\n</head><body>\n<h1>";
  html = html + header + "</h1>\n";
  html = html + body + "\n</body>\n</html>\n";
  return html;
}

void respondRequestWithLogEntries() {
  String header = "Debugging-Log-Entries";
  String body = "";
  body = "<h2>Logging on Startup / during configuration (Setup-Log)</h2>\n";
  body = body + setupLogText;
  body = body + "<h2>Logging during operation (Loop-Log)</h2>\n";
  body = body + loopLogText;
  server.send(200, "text/html; charset=utf-8", renderHtml(header, body));
}

void startOTA() {
  ArduinoOTA.onStart([]() {
      String type;
      if (ArduinoOTA.getCommand() == U_FLASH)
        type = "sketch";
      else  // U_SPIFFS
        type = "filesystem";
      // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
      debugOutput("Start OTA Firmware update " + type, 4, true);
    });

  ArduinoOTA.onEnd([]() {
      debugOutput("\nEnd OTA Firmware update ", 4, true);
    });

  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
      debugOutput("OTA Progress: " + String(progress / (total / 100)), 4, true);
    });

  ArduinoOTA.onError([](ota_error_t error) {
      debugOutput("Error " + String(error), 4, true);
      if (error == OTA_AUTH_ERROR)         debugOutput("Auth Failed", 2, true);
      else if (error == OTA_BEGIN_ERROR)   debugOutput("Begin Failed", 2, true);
      else if (error == OTA_CONNECT_ERROR) debugOutput("Connect Failed", 2, true);
      else if (error == OTA_RECEIVE_ERROR) debugOutput("Receive Failed", 2, true);
      else if (error == OTA_END_ERROR)     debugOutput("End Failed", 2, true);
    });

  ArduinoOTA.setHostname(OTA_HOSTNAME);
  ArduinoOTA.setPassword(OTA_PASSWORD);
  ArduinoOTA.begin();
}