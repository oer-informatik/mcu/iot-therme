# IoT-Erweiterung der Regelung einer Junker-Therme (Teil 1: Schaltung)

<span class="hidden-text">
https://oer-informatik.de/esp32-iot-heizung-hardware
</span>

[comment]: # (Den oberen und den unteren Teil kopieren)


> **tl/dr;** _(ca. 10 min Lesezeit): Eine Heizung mit nur einem Raumthermostat ist wenig flexibel, wenn dieser Raum mal nicht beheizt werden soll. Mithilfe eines ESP32 soll die vorhandene Regelung ergänzt werden um die Möglichkeit, weitere IoT-Thermostate auszulesen._

Das Gesamtprojekt ist in drei HowTos unterteilt: 1. die Hardware (dieser Teil), dazwischen [ein Exkurs zur ESP-Vorbereitung](https://oer-informatik.de/esp32-iot-basics) und [2. die zugehörige Software](https://oer-informatik.de/esp32-iot-heizung-software).

## Problemstellung

Heizungen können über ein Außenthermometer oder Raumthermostate geregelt werden. Bei kleineren Gasheizungen ("Gasetagenheizungen") wird das häufig durch ein einzelnes Raumthermostat umgesetzt, dass idealerweise in dem Raum mit dem größten Wärmebedarf hängt. Der Heizkörper in diesem Raum ist normalerweise ohne Thermostat oder mit einem Thermostat in größter Einstellung versehen.

In meinem Fall ist dieser Raum das Esszimmer, was selten - und nur kurzzeitig benutzt wird. Je nach Tageszeit sollen unterschiedliche Räume beheizt werden, sodass ein reines Umhängen des Raumthermostats in einen anderen Raum keine Lösung war. Es musste also eine andere Lösung her.

## Anforderungen an die neue Lösung

Hierbei stand im Vordergrund:

* Alle Heizkörper (bzw. mindestens einer je Raum) sollen mit _smarten_ Thermostaten ausgestattet werden, die auslesbar und fernbedienbar sind.

* Die Heizung sollte nur anspringen, wenn ein Thermostat im Haus sowohl Wärmebedarf signalisiert als auch ein geöffnetes Ventil hat (also Wärme abnimmt).

* Die Technik der Thermostate sollte möglichst energiesparend sein, damit sie mit Akkus/Batterien mindestens eine Saison betrieben werden können, ohne nachzuladen/zu wechseln.

* Die neue Steuerung sollte parallel zur bisherigen Steuerung installiert werden können. Sollte es bei den IoT-Komponenten Probleme geben, möchte ich nicht frieren. Zudem sollte der Frostschutz sicherheitshalber über die alte Steuerung laufen.

## Teilprojekt 1: Der elektronische Aufbau der neuen Regelung

### Disclaimer

In eine bestehende Heizungsregelung einzugreifen wirft natürlich einige Fragen auf: letztlich schaltet man mit Gas und Strom zwei nicht ganz ungefährliche Energieträger. Die entworfene Schaltung erzeugt bei Fehlern hohe Investitionskosten (die Heizungsanlage) oder hohe Betriebskosten (Gas). Ich bitte daher, die folgenden Absätze eher als Ideensammlung zu betrachten und im Zweifel einen Profi zurate zu ziehen!

### Ausgangslage: die bestehende Regelung

Ursprünglich war an die Heizung das recht einfache Raumthermostat Junckers TR-200 angeschlossen. Dieses kennt nur eine einfache Zeitschaltuhr und wurde von mir schnell durch das Junckers TR-100 ersetzt, was immerhin wochentagsspezifische Heizprofile und Urlaubsschaltung kennt.

Diese Regelung wird angeschlossen über drei Pins, die aus mir unbekannten Gründen mit 1-2-4 durchnummeriert werden:

* Pin 1 = 24V

* Pin 2 = Steuerleitung

* Pin 4 = GND

Pin 2 ist also das Herzstück, über den das Raumthermostat den Wärmebedarf an die Heizung weitergibt. Das Internet ist voller Mutmaßungen, welche Werte der Steuerleitung zu welchem Heizverhalten führen. Ich nenne hier einfach mal die Thesen, die ich gefunden hatte:

1. Pin 2  kleiner als 3V = aus; 24 V: 100% Heizleistung

2. Pin 2 0-3V: "nicht gut", 5V - 8V: Therme aus, 8-15V Regelbereich

3. umgekehrt: 0V: volle Leistung, 24V = aus (um es gleich zu sagen: in meinem Fall: NEIN!)

4. $3,2 \text{mA}$: Therme aus, 0,2mA = 100% (konnte ich so auch nicht bestätigen)

5. $7 \text{ kOhm}$ Pullup an der Therme zwischen Steuerleitung und 24V. Daraus folgt: wenn keine Steuerung angeschlossen gibt die Heizung 100%

These 1 und 2 und 5 liegen recht dicht beieinander und klangen am plausibelsten (wurden auch häufig genannt). Nach etwas Recherche hatte ich mich entschieden, einfach nachzumessen - und konnte so die am häufigsten geäußerte Vermutung bestätigen:

Messung bei 18° C Raumtemperatur

* Thermostat eingestellt auf 16,5°C: 340mV

* Thermostat eingestellt auf 17,5°C: 10,8V

* Thermostat eingestellt auf 18°C: 11-15V

* Thermostat eingestellt auf 18,5°C: 17-19V

* Thermostat eingestellt auf 19°C: 20V

Das Thermostat zieht demnach die Steuerleitung gegen 0V, wenn kein Wärmebedarf besteht. Oberhalb der 15V scheint ein Wärmebedarf signalisiert zu werden. Die Maximalspannung bei meiner Messung betrug 20V.

### Information zu bestehenden Lösungen und bestehenden Techniken

Wie immer quillt das Internet über mit Webseiten, die hierzu Ideen haben (seit heute: eine mehr). Die meisten Informationen zum Ersatz des Raumthermostats verdanke ich den folgenden Projektdarstellungen:

- per Shelly als WLAN-Schalter: [Alte Junkers Gastherme smart machen mit shelly(pattyland.de Blogeintrag)](https://blog.pattyland.de/2019/12/01/alte-junkers-gastherme-smart-machen/)

-  über ESP32 [von RobinMeis (robin.meis.space)](https://robin.meis.space/2019/07/03/junkers-therme-mit-wlan/) zugehöriges [Github-Repo](https://github.com/RobinMeis/Junkers_1-2-4_ESP32)

- als PC-Steuerung, auch per OP: [Schaltplan Platine für Steuerung ZSBE 16-3](http://www.roter-unimog.de/p4/46-hzg-technik.htm)

- per DAC und OP über FHEM: [Wiki-Eintrag](https://wiki.fhem.de/wiki/Junkers_Therme_Stetigregelung)

- per LTC1257 DAC und OP, Ergänzung zu vorgenanntem: [hier](https://www.ethersex.de/index.php/FHEM_Kueche_(Deutsch))
- Aufbau über BC547 Transistor [tahina.priv.at](http://www.tahina.priv.at/electronics/junkers.html)

Darüber hinaus gab es noch Lösungen über Optokoppler, Schaltungen mit eigenen Relais und und und...


### Anforderungen an den Schaltungsaufbau:

Bevor ich das Layout überlege, sollte ich noch einmal präzisieren, wie ich mir das wünsche:

- Die alte Regelung soll parallel laufen - daher sollte die neue Regelung mit einer Diode von der Steuerleitung getrennt werden.

- Die Logik und Schaltung soll später ein ESP32 übernehmen (dann bleibe ich flexibel, was die Wahl der Technik angeht). Details, woher dieser seine Informationen bekommt, folgen unten.

- Der ESP32 soll direkt über die 24V des 1-2-4-Busses betrieben werden.

- Die 0-24V für die Steuerleitung soll über den Analog-Out des ESP erzeugt werden (ich habe mich für eine OP-Variante entschieden).

- Alle Leitungen sollten intern mit Schmelzsicherungen abgesichert werden: _better be safe than sorry_

- LEDs sollen den Zustand der Heizung signalisieren (Heizung aus, an, maximal).

- Ein Temperaturfühler für Vor- und Rücklauftemperatur, z.B. DS 18S20: wäre nett, brauche ich aber nicht.

- 3-poliger Schalter zur kompletten Trennung der neuen Regelung: wäre gut, falls mal was nicht klappt (Spoiler: habe ich nicht realisiert)

- Ein Temperaturfühler im Raum wäre nett, aber ich will ja ohnehin nur dann die Heizung aktivieren, wenn ein Ventil offen ist. Ich brauche ihn also nicht.

Die Informationen für den Aufbau wären so weit zusammengetragen, aus den vielen Möglichkeiten habe ich mich für meine Richtung entschieden (ESP32 & OP). Es wird Zeit, die Schaltung zu entwerfen!

## Entwurf der Schaltung

Basierend auf den obigen Quellen habe ich mich für eine Schaltung auf Basis eines ESP32 entschieden, der per OP die gewünschte Spannung an der Steuerleitung anlegt.

Ich habe die Schaltung per Falstad ([bearbeitbarer Link](https://www.falstad.com/circuit/circuitjs.html?ctz=CQAgzCAMB0l3BWEBGGAmOaDsWyQBxoBsAnCViApJZdQgKYC0yyAUAE4hFoAsIG+SkWoCoKeJA5csaFFiIgZ1ZPLGoJUniQW86PPrrVxWAY0olZhhBfCQ+y2BKdPkTCj2hkv3rSRayYODYzBGEUNEFQkQI1R2d410YKNDBPciJkDJ5kEkIsZEFAyDYeSBIuXn5KrDsqvmQoaAVqQJT1eIlLVgA3FEzBCMEWIgHSsWoxypbGhFYAdxB8MEtK-Dg6qHmK+tVuAxjJABNFMB1q-ANK2UP6ADMAQwBXABsAFy3cBTw+IlDbe1YpT4WAu4UEa2UEXGsAyECI0FwYgezwAzvQtr8FDlZJiwZsFrjkFCQfUoZIFktZN8QH4qbVyYoMHIFEoNgzaf9xDFqZIVLItDoxjw1vwxtQKEgkNMYEgAGoAeze9wA5uiFtZLGMEPpRS0tlE8YSyfqwtSDaJJGAaotlht8CK0JUGnqVPVIH8iUM4AMYg12usYGQqB14A0ACoAC3o7AAtvKUa97u8KSLPeIPcaFqzHcDTmypP4RE6iZDCop4Ft7UW+Ol8wt1AMnd6UCN8X0mWnUBnCqwAA42lZ8Smc52bZV9VvU4aCZbS1gADxpZBpXu4E5ApJA9ze9BRAB09wA7SPRuMJpML9tY7QTv3LzcABRez0evcvFggJZXn5SG-CotlPcUQACgfABLQ8gOQABKS8eCICBiFkBAqH4EVNwAUQg5V7kPZUURRMDlUPbcD0glEnn3FEAFkAGEAFV32QJA0BvXIGlY8pNwAcQAOQAESA0CIKAnhYMXPxEPdFczggTcAGVXnoR5o2eegwNeR48LIoTwPItBYPrfoRy-HktgbDZCzrdNBWUd0vnpcz7JMpkzPrZyp2bNztl1cxNT1dyYhzGlVGChkNU5XFvNxNA-iixz1VNWoIu87NqlqMK+xOM5cwc+pxlYAAjPp8HKfAsVQCAwDAAFirICh7XEEhqGq5pAVqEk7XWMLGkgWEuARCBqGRNEisUchFkEHhtXAJkGWHNMITxS1rSrPEFqhUcGWnPEdsMSRFxYSAKGXFhUNQfl-xPWN0UXaaKroHI8U3ABJeUwzI66z0TZMJwGRqdu8nbOy-NMGU+Tlh2ip1dhkZk2y7WybP4A5WHlOQuD4OSyimlo4jKeRQlisRZGYzYgA), [Link zur Datei](falstad-sim.txt)) entworfen. Links ist der Bereich der neuen Regelung als Box dargestellt: Der ESP32 legt ein Potenzial an die Leitung ganz links an "Eingangssignal aus MCU". Daneben folgt die OP-Schaltung, danach die Diode, die den gemeinsamen Betrieb von alter und neuer Steuerung absichert. An der Grenze der neuen Steuerung sind die Sicherungen.

Rechts daneben als Box findet sich das alte bestehende Thermostat. Ich habe es vereinfacht als Poti simuliert, dass die Steuerleitung auf 0V oder 4V zieht.

Ganz rechts liegt schließlich die Box, die die Therme repräsentiert. Die gemessenen 7kOhm Pullup habe ich neben der Spannungsquelle als einzige Bauteile hierfür angenommen.

![Schaltplan der neuen Steuerung](images/ohne-messung.png)


Diese Konstellation habe ich für drei möglichen Betriebsszenarien simuliert:


1. Das alte Thermostat meldet Wärmebedarf, der ESP nicht: Die Diode sperrt.

![Schaltplan der neuen Steuerung, alte Regelung zieht die Steuerleitung auf 23V](images/altesThermostatbestimmt.png)


2. Sowohl altes Thermostat als auch ESP melden keinen Wärmebedarf:

![Schaltplan der neuen Steuerung, die Steuerleitung liegt auf 18mV](images/thermeaus.png)

3. Das alte Thermostat meldet keinen Wärmebedarf, der ESP aber schon: Die Diode leitet.

![Schaltplan der neuen Steuerung, neue Regelung zieht die Steuerleitung auf 20V](images/neuesThermostatbestimmt.png)

Das sieht ja schonmal ganz erfolgversprechend aus. Nächster Schritt: Bauteile bestellen!

### Bauteilliste

Folgende Bauteile habe ich benötigt:

- 24V Schmelzsicherungen (3 Stück, $100mA$)
- Spannungsregler z.B. [LM2596S](https://www.az-delivery.de/products/lm2596s-dc-dc-step-down-modul-1)
- Kondensatoren 2x $100 nF$
- Widerstände $100 \Omega$, $10 k\Omega$, $100 k\Omega$
- Diode, $24V$, $100mA$ (z.B.: 1N4001)
- Operationsverstärker, z.B. LM358
- ESP32
- Lochrasterplatine


### Aufbau auf der Lochrasterplatine

Die bestehenden Komponenten passen gerade auf eine 7x9cm Lochrasterplatine. Auch ohne große Lötbegabung habe ich eine funktioniernde Schaltung aufbauen können. Das erstellte Fritzing-Schema hilft sehr bei der Positionierung der einzelnen Bauteile. Unter diesem Link findet sich die [Fritzing-Datei in bearbeitbarer Form](1-2-4_Sketch.fzz).

![Entwurf der Lochrasterplatine mit Fritzing](images/fritzing_aufbau.png)

Tatsächlich bin ich mit dem realisierten Aufbau für meine Begriffe recht nah am Entwurf geblieben:

![Realisierte Platine](images/aufbau.jpg)

### Testen der Platine

Da die Platine am Ende mit der Heizung verbunden wird, ist die korrekte Funktion sehr wichtig. Ich habe daher gewissenhafter als bei anderen Arduino-Projekten die einzelnen Potenziale nachverfolgt und die jeweiligen Widerstandswerte gemessen. Bloß gut: in einem Fall hatte mit der Perspektivwechsel (Draufsicht in Fritzing vs. Druntersicht beim Löten) ein Schnippchen geschlagen.

Nach dem Anlegen der Betriebsspannung (ohne installierte Software) wurde dann nochmal gemessen. Na klar: eine LED war verkehrt herum. Wir oft muss sowas eigentlich passieren?!?

Trotz allem: eingebaut hab ich die Platine erst, nachdem ich mit aufgespielter Software die einzelnen Szenarien am Labornetzteil durchgemessen hatte.

## Fazit

Der elektronische Teil des Projekts ist somit abgeschlossen. Wenn man sich dann erst einmal für eine Lösung entschieden hat, ist der Aufbau der Platine gar nicht so schwer. Im Wissen, dass diesmal meine warme Wohnung zur Disposition steht, war ich beim Aufbau doch etwas nervöser als üblich.

Die Simulation mit Falstad und der Entwurf des Aufbaus mit Fritzing haben mir eine ganze Menge Probleme erspart. Denn ein paar Nerven müssen ja noch übrig bleiben für die Software-Seite des Projekts...

![Die aufgebaute und (provisorisch) angeschlossene Platine, darunter die ursprüngliche Regelung](images/aufbau_mit_fr-100.jpg)


## Links und weitere Informationen

- [Alte Junkers Gastherme smart machen mit Shelly (pattyland.de Blogeintrag)](https://blog.pattyland.de/2019/12/01/alte-junkers-gastherme-smart-machen/)

- Lösung [von RobinMeis (robin.meis.space)](https://robin.meis.space/2019/07/03/junkers-therme-mit-wlan/) zugehöriges [Github-Repo](https://github.com/RobinMeis/Junkers_1-2-4_ESP32)

- [Schaltplan Platine für Steuerung ZSBE 16-3](http://www.roter-unimog.de/p4/46-hzg-technik.htm)
- [Wiki-Eintrag](https://wiki.fhem.de/wiki/Junkers_Therme_Stetigregelung)

- Infos Stand 30.10.2018 [Ansteuerung Junkers 1-2-4-Schnittstelle (mikrocontroller.net)](https://www.mikrocontroller.net/topic/450569)

- Artikel im [FHEM-Forum](https://forum.fhem.de/index.php?topic=18101.0)

- [tahina.priv.at](http://www.tahina.priv.at/electronics/junkers.html)

## _Quellen und offene Ressourcen (OER)_

Die Ursprungstexte (als Markdown), Grafiken und zugrunde liegende Diagrammquelltexte finden sich (soweit möglich in weiterbearbeitbarer Form) in folgendem git-Repository:

[https://gitlab.com/oer-informatik/mcu/iot-therme](https://gitlab.com/oer-informatik/mcu/iot-therme).

Sofern nicht explizit anderweitig angegeben sind sie zur Nutzung als Open Education Resource (OER) unter Namensnennung (H. Stein, oer-informatik.de) freigegeben gemäß der [Creative Commons Namensnennung 4.0 International Lizenz (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/deed.de).

[![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by/4.0/88x31.png)](https://creativecommons.org/licenses/by/4.0/deed.de)
